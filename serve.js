var express = require("express");
var path = require("path");
var body_parser = require("body-parser");

var tasks   = require("./routes/tasks");

var app = express();
var port = 3000;


//bodyparser
app.use(body_parser.json());
app.use(body_parser.urlencoded({extended: false}));

app.use("/api", tasks);

app.listen(port, function(){
    console.log("serve start port "+ port);
})