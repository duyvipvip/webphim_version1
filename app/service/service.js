mainapp.factory("loaddata_service",function($http, $window, $q){
     return {
        getData: function() {
            var defer = $q.defer();
            $http.get('http://localhost:3000/api/tasks', { cache: 'true'})
            .then(function(response) {
                defer.resolve(response);
            });
            return defer.promise;
        }
     } 
        // return {
        //     getData: function(){
        //         return new Promise((resolve, reject) => {
        //              $http.get('http://localhost:3000/api/tasks', { cache: 'true'})
        //             .then(function(response) {
        //                 resolve(response);
        //             });  
        //         });
        //     }
        // }
});


mainapp.factory("getusers",function($http, $window, $q){
     return {
        getuser: function() {
            var defer = $q.defer();
            $http.get('http://localhost:3000/api/users', { cache: 'true'})
            .then(function(response) {
                defer.resolve(response);
            });
            return defer.promise;
        }
     }
});

// // set cookies đăng nhập
// mainapp.factory('$cookieStore', ['$cookies', function($cookies) {
//     return {
//         get: function(key) {
//             return $cookies.getObject(key);
//         },
//         put: function(key, value) {
//             $cookies.putObject(key, value);
//         },
//         remove: function(key) {
//             $cookies.remove(key);
//         }
//     }
// }]);

//kiểm tra tài khoản
mainapp.factory('checklogin', function() {
    return {
        check: function(user, taikhoan, matkhau) {
            let n = user.length;
            for(var i=0; i< n;i++){
                if(user[i].taikhoan == taikhoan && user[i].matkhau == matkhau){
                    return user[i];
                }
            }
            return "";
        }
       
    }
});
//lấy username
mainapp.factory('getusername', () => {
    return {
        get: (user) => {
            return user[0].hoten;
        }
    }
});
//dang ki
mainapp.factory('dangki_sevice', ($http, $httpParamSerializerJQLike) => {
    return{
        dangki: (newuser) => {
            $http({
                url: 'http://localhost:3000/api/user',
                method: 'POST',
                data: $httpParamSerializerJQLike(newuser),
                headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
                }
            });

        }
    }
});

mainapp.factory("menu_service",function($http, $window, $q){
     return {
        getData: function() {
            var defer = $q.defer();
            $http.get('http://localhost:3000/api/menu', { cache: 'true'})
            .then(function(response) {
                defer.resolve(response);
            });
            return defer.promise;
        }
     } 
});

mainapp.factory("theloai_service",function($http, $window, $q){
     return {
        getData: function() {
            var defer = $q.defer();
            $http.get('http://localhost:3000/api/theloaiphim', { cache: 'true'})
            .then(function(response) {
                defer.resolve(response);
            });
            return defer.promise;
        }
     } 
});

mainapp.factory("getphim_service",function($http, $window, $q){
     return {
        getData: function(id) {
            var defer = $q.defer();
            $http.get('http://localhost:3000/api/thongtinphim/'+id, { cache: 'true'})
            .then(function(response) {
                defer.resolve(response);
            });
            return defer.promise;
        }
     }
});


