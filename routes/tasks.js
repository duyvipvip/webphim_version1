var express     = require("express");
var route       = express.Router();
var mongojs     = require("mongojs");
var db          = mongojs("mongodb://havanduy:havanduy@ds047095.mlab.com:47095/databasefilm");

route.get("/tasks", function(req, res, next){
    db.phim.find(function(err, phim){
        if(err){
            res.send(err);
        }
        res.json(phim);
    });
});
route.get("/users", function(req, res, next){
    db.user.find(function(err, user){
        if(err){
            res.send(err);
        }
        res.json(user);
    });
})

route.post('/user', function(req, res, next){
    var task = req.body;
    db.user.save(task, function(err, task){
        if(err){
            res.send(err);
        }
        res.json(task);
    });
});
route.get('/theloaiphim', function(req, res, next){
    var task = req.body;
   db.theloaiphim.find(function(err, theloaiphim){
        if(err){
            res.send(err);
        }
        res.json(theloaiphim);
    });
});

route.get('/menu', function(req, res, next){
    var task = req.body;
   db.menu.find(function(err, menu){
        if(err){
            res.send(err);
        }
        res.json(menu);
    });
});

// get single
route.get("/thongtinphim/:id", function(req, res, next){
   db.phim.findOne({_id: mongojs.ObjectId(req.params.id)},function(err, phim){
        if(err){
            res.send(err);
        }
        res.json(phim);
   });
})
module.exports = route;