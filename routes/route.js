mainapp.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl: "app/page/trangchu/trangchu.html",
        controller  : "trangchu_controller"
    })
    .when("/dangnhap", {
         templateUrl: "app/page/dangnhap/dangnhap.html",
        controller  : "dangnhap_controller"
    })
    .when("/thongtinphim/:id", {
        templateUrl : "app/page/thongtinphim/thongtinphim.html",
        controller  : "thongtinphim_controller"
    })
    .when("/xemphim/:id", {
        templateUrl : "app/page/xemphim/xemphim.html",
        controller  : "xemphim_controller"
    })
    .when("/dangki", {
        templateUrl : "app/page/dangki/dangki.html",
        controller  : "dangki_controller"
    });
   
});
